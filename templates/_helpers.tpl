{{/*
Expand the name of the chart.
*/}}
{{- define "MyAppCtx.chartName" -}}
{{- default .Chart.Name | trunc 24 | trimSuffix "-" }}
{{- end }}

{{/*
Create chart name and version as used by the chart label.
*/}}
{{- define "MyAppCtx.chartNameVersion" -}}
{{- printf "%s-%s" .Chart.Name .Chart.Version | replace "+" "_" | trunc 63 | trimSuffix "-" }}
{{- end }}

{{/*
Application name
*/}}
{{- define "MyAppCtx.fullname" }}
{{- $name := default .Chart.Name .Values.name -}}
{{- printf "%s" $name | trunc 30 | trimSuffix "-"}}
{{- end }}

{{/*
API Name
*/}}
{{- define "MyAppCtx.api.name" }}
{{- printf "%s-api" (include "MyAppCtx.fullname" .) }}
{{- end }}

{{/*
Front Name
*/}}
{{- define "MyAppCtx.front.name" }}
{{- printf "%s-front" (include "MyAppCtx.fullname" .)  }}
{{- end }}

{{/*
DB Name
*/}}
{{- define "MyAppCtx.db.name" }}
{{- printf "%s-pg" (include "MyAppCtx.fullname" .) }}
{{- end }}

{{/*
DB Name
*/}}
{{- define "MyAppCtx.db.service" }}
{{- printf "\"%s.quentin-miguel-lopez:5432\"" (include "MyAppCtx.db.name" .) }}
{{- end }}